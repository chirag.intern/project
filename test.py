import re
import os
import json
import paramiko
from datetime import date,timedelta,datetime
import bernhard
import sys
import argparse
from collections import defaultdict

# datetime object containing current date and time
today = datetime.now()

#These return the current year , month ,presentday
year = (today.strftime("%Y"))
month = (today.strftime("%m"))
presentday = (today.strftime("%d"))

#bernhard Clients helps in sending events to Riemann.
riemann = bernhard.Client()

#These variables stores yesterday and day before yesterdays date
day_before_date = int(presentday)-2
yesterday = int(presentday)-1

# construct the argument parser 
parser = argparse.ArgumentParser(description='Compares N number of servers')
# Add an argument
parser.add_argument('number',type=int, help="Number represents day" , nargs='?' , default=int(day_before_date) , const="num")
parser.add_argument('-i', dest='file_path',nargs='?', help = 'Takes a file in command line argument',const = 'filepath',required=False)
parser.add_argument('-s', help = 'File_size',required=False,nargs='*' )

# Parse the argument
args = parser.parse_args()

#When you enter a number in with command line argumnet , the number gets stored in check , or else it takes a default number that is present date - 2
check = args.number

#This block will help you to get the size of the folders and tells which clients are present in whih servers which have log entries in them, when you run the command  : 
#USAGE:"python3 test.py -s /path/to/json"
#This code defaults take only presentdate -1 and presentdate-2 and gives you output according to it.
if args.s:
  #This function gives the file/folder size within a dir of the local server
  def a():
    sizes = []
    sub_name_list = []
    for root, dir_, files in os.walk(local):
      for f  in dir_:
        fp = os.path.join(os.path.abspath(root), f)
        size = os.path.getsize(fp)
        sizes.append(f'{f}:{os.path.getsize(fp)}')
    print(" ")
    print(f"DATE:{date} The clients size in server1 is {sizes}") 

  #This function gives the file/folder size within a dir of the other backup servers(or can be called as remote machine for understanding)
  def b():
    for i in iter(_stdout1.readline,""):
      tot_size = sum(list(map(int, re.findall('\d+', i))))
      print(f"DATE {date} :  The client size in {username} is {tot_size}  kb")
  
  try:
    with open(sys.argv[-1],'r') as file:
      data=json.load(file)
  except IndexError:
    print("You need to specify the path to be listed")

#This block will tell you which clients are present in which servers from a day you enter to the presentdate-1
#USAGE : 'python3 test.py -i /path/to/json/ day'
#Eg: 'python3 test.py -i /path/json/ 5' It will start giving the us clients entries present in different servers from "5" to "presentdate-1"
elif len(sys.argv)>3:
  args = parser.parse_args()
  check = args.number
  try:
    with open(args.file_path,'r') as file:
      data=json.load(file)
  except IndexError:
    print("You need to specify the path to be listed")

#This block will tell you which clients are present in which servers from presentdate-2 to presentdate-1
#USAGE : 'python3 test.py -i /path/to/json/'
else:
  try:
    with open(args.file_path,'r') as file:
      data=json.load(file)
  except IndexError:
    print("You need to specify the path to be listed")

#This helps in Iterating through a range of dates in Python
def daterange(startdate , enddate):
         for n in range(int((enddate-startdate).days)):
                 yield startdate+timedelta(n)
#Ends the loop when presentdate-1 is reached                 
enddate = date(int(year) , int(month),int(presentday))
#Start the loop from the day we enter or take default as presentdate-2(as we used check variable)
startdate = date(int(year),int(month),check)

previous =False
state = True
is_critical = False
date_all_clients = {}
is_two_dates = (int(presentday) - int(check)) <= 2

for single_date in daterange(startdate , enddate):
    date = single_date.strftime("%d")
    
    #path of your clients in local server
    local= f"/home/server/Desktop/backupdestination/{year}/{month}/{date}/"
   
    #path of your clients in remote servers
    remote =[f"/home/server2/Desktop/backupdestination/{year}/{month}/{date}/", f"/home/server3/Desktop/backupdestination/{year}/{month}/{date}/" ]
    
    #This gets the name of immediate folder for local path
    local_files = set(os.path.join(local, filename)[len(local):] for path,dirs,files in os.walk(local) for filename in dirs)

    #As we have used Asymetric encryption for connecting to our remote servers using paramiko library , we are specifying path/to/key
    path_to_key = data["path"]
    key=paramiko.RSAKey.from_private_key_file(path_to_key)
    
    #all_files stores all local_files in it
    all_files={}
    all_files['server1'] = local_files
    
    #When ever we parse a command line with -s , function a() is being called
    if args.s:
      a()

    #This will loop through all the servers which are present in json file
    for remotes,host,username in list(zip(remote , data["host"] ,data["username"])):
      #This gets all the immediate sub dir from remote machine(or other backup machines)
      code = f'local = {remotes!r}; import os,json,sys; json.dump([os.path.join(path, filename)[len(local):] for path , dirs , files in os.walk(local) for filename in dirs],sys.stdout)'
      #This gets the folder size of remote path from remote machine(or other backup machines)
      code1 = f'local = "{remotes!r}"; import os,json,sys; json.dump([os.path.getsize(os.path.join(path, filename)) for path, dirs, files in os.walk(local)for filename in dirs],sys.stdout)'
      
      #This helps in conneting to the other servers
      client = paramiko.client.SSHClient()
      client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
      client.connect(host,username=username,pkey=key)
      
      #This will execute in other servers and get the size of the folder
      _stdin1,_stdout1,_stderr1 = client.exec_command(f"python3 -c '{code1}'")

      #When ever we parse a command line with -s , function b() is called
      if args.s:
        b()
      #This will execute a run in other servers and get the name of the sub dir of a path(remote is path)
      _stdin, _stdout, _stderr = client.exec_command(f'python3 -c "{code}"')
    
      #remote_files stores all the CLIENTS name present only in remote servers
      remote_files = set(json.load(_stdout))
      all_files[username] = remote_files
    
    #all_clients stores all the CLIENTS name which are present in all the servers
    all_clients = set.union(*all_files.values())

    #This condition helps in comparing presentdate-1 and prsentdate-2 CLIENTS
    if is_two_dates:
       date_all_clients[int(date)] = all_clients

    #good_servers are servers which have CLIENTS stored in them
    good_servers = set(server for server, clients in all_files.items() if clients == all_clients)
  

    #if else condition checks for the CLIENTS are present in all SERVERS or not
    #This condtion checks if there are no CLIENTS in them.
    if len(all_clients) == 0:
      print(f"DATE {date} All the servers has no clients present in them ", end=" ")
    
    elif good_servers != all_files.keys():
      #This condtion compares the CLIENTS present in presentdate-1 and presentdate-2.
      if (previous or (previous is not False and previous == 0)) and is_two_dates:
         #Previous_clients stores the clients present in presentdate-2 and current_clients_stores the clients present in presentdate-1
         previous_clients = date_all_clients[(int(date)-1)]
         current_clients = date_all_clients[(int(date))]
         result = set(previous_clients) - set(current_clients)
         if previous_clients > current_clients:
           print(f"DATE {date} {result} is missing from all servers " ,end=' ')
           print(f" {all_clients} are in {good_servers} " ,end=" ")
         if len(previous_clients)==0 and len(current_clients)==0:
           print(f"DATE {date} No clients are in servers ",end=" ")
         if previous_clients  == current_clients:
           print(f"DATE {date} : {all_clients} are in {good_servers} ",end=" ")
      else:
         print(f"DATE {date}: {all_clients} are in  {good_servers} ", end=" ")
         
    #If there are servers which conatins CLIENTS then it enters this block
    elif good_servers:
      print(f"DATE {date}: {all_clients}  are in {good_servers} ", end="")
      previous_clients = date_all_clients[(int(date)-1)]
      current_clients = date_all_clients[(int(date))]
      result = set(previous_clients) - set(current_clients)
      if previous_clients > current_clients:
           print(f"{result} is missing from all the servers " ,end=' ')
    for i, clients  in enumerate(set(frozenset(clients) for clients in all_files.values() if clients != all_clients)):
      if i > 0:
       print(f"", end="")
      print(f"{all_clients - clients} are missing on {list(server for server, f in all_files.items() if f == clients)} ",end="")
    print(".")

    state = len(all_clients)

    #This block compares last 2 dates (presentdate -1 and presentdate-2) and check CLIENTS are present in all SERVERS or not , and sends us the events according to comparision
    if (previous or (previous is not False and previous == 0)) and is_two_dates:
      previous_clients = date_all_clients[int(date)-1]
      current_clients = date_all_clients[ int(date) ]
      result3 = set(previous_clients) - set(current_clients)
      if previous > state:
        print("is critical")
        print(f'Because There are no {result3} in {yesterday}  comapred to {day_before_date} so the state is critical ')
        riemann.send({'host': 'server', 'service': 'chirag','state':'critical'})

      if len(previous_clients)==0 and len(current_clients)==0:
        print("")
        print(f"There are no clients on DATE : {day_before_date} and DATE : {yesterday}")
        print("STATE : is Okay")
        riemann.send({'host': 'server', 'service': 'chirag','state':'OK'})

      if previous_clients == current_clients:
        print("")
        print(f"DATE :{yesterday} and DATE :{day_before_date} have equal number of clients , so the State will be OK")
        print("STATE : is Okay")
        riemann.send({'host': 'server', 'service': 'chirag','state':'OK'})
    previous= state

